//
//  MapViewController.swift
//  BackCountryExplorers
//
//  Created by Test on 2017-11-03.
//  Copyright © 2017 ASPL. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

//    var btnZoomIn = UIButton()
    var currentZoom:Float = 2.0
    
    
    @IBOutlet var MapView: GMSMapView!
    
    let locationManager = CLLocationManager()
    var btnZoomOut = UIButton()		
    override func viewDidLoad() {
        super.viewDidLoad()
//        btnZoomIn = self.view.viewWithTag(2) as! UIButton
        
//        btnZoomOut = UIButton.init(frame: CGRect.init(x: btnZoomIn.frame.origin.x, y: btnZoomIn.frame.origin.y + btnZoomIn.frame.height, width: 40, height: 40))
//        btnZoomOut.alpha = 0.75
//        btnZoomOut.backgroundColor = UIColor.white.withAlphaComponent(0.45)
//        btnZoomOut.imageEdgeInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)
//        btnZoomOut.addTarget(self, action: #selector(self.zoomOut), for: .touchUpInside)
//        btnZoomOut.setImage(#imageLiteral(resourceName: "zoom_out"), for: .normal)
        
        MapView.isMyLocationEnabled = true
        MapView.settings.compassButton = true
        MapView.settings.myLocationButton = true

        if (CLLocationManager.locationServicesEnabled())
        {
            self.locationManager.delegate=self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }

        MapView.delegate = self
//        self.view.addSubview(btnZoomOut)
    }

//    @objc func zoomOut(sender: UIButton) {
//        btnZoomIn.alpha = 0.75
//        currentZoom = self.MapView.camera.zoom
//        if currentZoom > 2 {
//            currentZoom = currentZoom - 1;
//            print("Minus")
//            MapView.animate(toZoom: currentZoom)
//        }
//        if currentZoom == 2 {
//            sender.alpha = 0.45
//        }
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("Position Changed")
        
        if self.MapView.camera.zoom <= 20 {
           btnZoomIn.alpha  = 0.75
        }else {
            btnZoomIn.alpha  = 0.45
        }
        if self.MapView.camera.zoom <= 2 {
            btnZoomOut.alpha = 0.45
        }else{
            btnZoomOut.alpha = 0.75
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations.last
        let cam = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude,
                                              longitude: userLocation!.coordinate.longitude, zoom: 13.0)
        MapView.camera = cam
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake((userLocation?.coordinate.latitude)!, (userLocation?.coordinate.longitude)!)
        marker.title = "Ahmedabad"
        marker.snippet = "Gujarat"
        marker.map = MapView
        
        locationManager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
      
        let advDetVC = self.storyboard?.instantiateViewController(withIdentifier: "AdventureDetailsViewController") as! AdventureDetailsViewController
        self.slideMenuController()?.changeMainViewController(advDetVC, close: true)
    }
    



//    @IBAction func btnZoomIn(_ sender: UIButton) {
//        btnZoomOut.alpha = 0.75
//        currentZoom = self.MapView.camera.zoom
//        if currentZoom < 21 {
//            currentZoom = currentZoom + 1;
//            print("Plus")
//            MapView.animate(toZoom: currentZoom)
//        }
//        if currentZoom == 21 {
//            sender.alpha = 0.45
//        }
//    }
    
    @IBAction func btnAddAdventure(_ sender: Any) {
        let addAdvVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAdventureViewController") as! AddAdventureViewController
        self.slideMenuController()?.changeMainViewController(addAdvVC, close: true)
    }
    
    @IBAction func btnMenuClick(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
}
