//
//  SignUpViewController.swift
//  BackCountryExplorers
//
//  Created by Test on 2017-10-31.
//  Copyright © 2017 ASPL. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtContact: UITextField!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alert(title: String, message: String, actionTitle: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: actionTitle, style: .default, handler: nil))
        present(alertView, animated:true, completion:nil)
    }
    
    @IBAction func btnSignUp(_ sender: UIButton) {
        if !validateName(name: txtName.text!){
//            alert(title: "", message: <#T##String#>, actionTitle: <#T##String#>)
            let alertView = UIAlertController(title: "Invalid Name", message: "Try Again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated:true, completion:nil)
        }else if !validateEmail(email: txtEmail.text!){
            let alertView = UIAlertController(title: "Invalid Email", message: "Try Again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated:true, completion:nil)
        }else if !validatePhone(phone: txtContact.text!){
            let alertView = UIAlertController(title: "Invalid Phone", message: "Try Again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated:true, completion:nil)
        }else if !validateLocation(location: txtLocation.text!){
            let alertView = UIAlertController(title: "Invalid Location", message: "Try Again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated:true, completion:nil)
        }else if !validatePassword(password: txtPassword.text!){
            let alertView = UIAlertController(title: "Invalid Password", message: "Try Again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated:true, completion:nil)
        }else if !(txtConfirmPass.text == txtPassword.text){
            let alertView = UIAlertController(title: "Password do not match", message: "Try Again", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated:true, completion:nil)
        }
        else{
            navigationController?.popViewController(animated: true)
        }
      
    }

    
    @IBAction func btnLogin(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func validateName(name: String) -> Bool {
//        let nameRegex = "[A-Z0-9a-z._]"
//        return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: name)
        return true
    }
    
    func validateEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    
    func validatePhone(phone: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluate(with: phone)
    }
    
    func validateLocation(location: String) -> Bool {
        let locationRegex = "[A-Za-z]"
        return NSPredicate(format: "SELF MATCHES %@", locationRegex).evaluate(with: location)
    }
    
    func validatePassword(password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z0-9])[0-9A-Za-z\\d$@$#!%*?&]{4,}")
        return passwordTest.evaluate(with: password)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
