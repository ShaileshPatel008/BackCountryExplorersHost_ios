//
//  AdventureDetailsViewController.swift
//  BackCountryExplorers
//
//  Created by Test on 2017-11-06.
//  Copyright © 2017 ASPL. All rights reserved.
//

import UIKit

class AdventureDetailsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var btnPrev1: UIButton!
    @IBOutlet var btnNext1: UIButton!
    @IBOutlet var lblNext: UILabel!
    @IBOutlet var lblPrev: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblPrev.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AdvCollectionViewCell
//        cell.backgroundColor = UIColor.white
        cell.imgAdv.image = #imageLiteral(resourceName: "Aerial01")
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell!")
    }

    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        if currentItem.row == 0{
            lblPrev.isHidden = true
            lblNext.isHidden = false
        }else{
            lblPrev.isHidden = false
        }
        if currentItem.row == 4{
            lblPrev.isHidden = false
            lblNext.isHidden = true
        }else{
            lblNext.isHidden = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = collectionView.frame.size.width
        let height = collectionView.frame.size.height
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
        
    }

    @IBAction func btnNext(_ sender: UIButton) {
        
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        // This part here
        lblPrev.isHidden = false
        if nextItem.row < 5 {
            self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)
        }
        if nextItem.row == 4{
            lblNext.isHidden = true
        }
    }
    
    @IBAction func btnPrev(_ sender: UIButton) {
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let prevItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        // This part here
        print(prevItem)
        lblNext.isHidden = false
        if prevItem.row >= 0 {
//            lblPrev.isHidden = false
            self.collectionView.scrollToItem(at: prevItem, at: .right, animated: true)
        }
        if prevItem.row == 0{
            lblPrev.isHidden = true
        }
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.slideMenuController()?.changeMainViewController(mapVC, close: true)
    }
}
