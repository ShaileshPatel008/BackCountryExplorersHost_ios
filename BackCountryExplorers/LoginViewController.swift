//
//  LoginViewController.swift
//  BackCountryExplorers
//
//  Created by Test on 2017-10-31.
//  Copyright © 2017 ASPL. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import Firebase
import SkyFloatingLabelTextField
import SlideMenuControllerSwift
//import Alamofire

class LoginViewController: UIViewController, GIDSignInUIDelegate, UITextFieldDelegate {
    
    @IBOutlet var txtEmail: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet var txtPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet var btnLogin: UIButton!
    
    var dict : [String : AnyObject]!
    var showPass : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogin.layer.cornerRadius = 10
        btnLogin.clipsToBounds = true
        
        txtEmail.errorColor = UIColor.red
//        self.navigationController?.navigationBar.isHidden = true
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(pushVC),
            name: NSNotification.Name(rawValue: "PushViewController"),
            object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnLogin(_ sender: UIButton) {
        if !validateEmail(email: txtEmail.text!) {
//            txtEmail.title = "Enter valid Email"
//            txtEmail.titleColor = UIColor.red
//            let alertView = UIAlertController(title: "", message: "Enter valid Email", preferredStyle: .alert)
//            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
//            present(alertView, animated:true, completion:nil)
        }else if !validatePassword(password: txtPassword.text!){
            let alertView = UIAlertController(title: "", message: "Enter valid Password", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertView, animated:true, completion:nil)
        }else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
            let slideMenu = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            UIView.transition(with: appDelegate.window!, duration: 1.0, options: .transitionFlipFromRight, animations: {
                appDelegate.window?.rootViewController = slideMenu
            })
//            let MapVC = storyboard?.instantiateViewController(withIdentifier: "MapViewController")
//            self.navigationController?.pushViewController(MapVC!, animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        if textField == txtEmail{
            if !validateEmail(email: txtEmail.text!){
                txtEmail.errorMessage = "Invalid Email"
            }else{
                txtEmail.errorMessage = nil
            }
        }
//        if let text = txtEmail.text {
//            if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
//                if(text.characters.count < 3 || !text.contains("@")){
//                    floatingLabelTextField.errorMessage = "Enter valid Email"
//                }
//                else {
//                    // The error message will only disappear when we reset it to nil or empty string
//                    floatingLabelTextField.errorMessage = ""
//                }
//            }
//        }
    }
    
    @IBAction func btnSignUp(_ sender: UIButton) {
        let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
        self.navigationController?.pushViewController(signUpVC!, animated: true)
    }
    
    @IBAction func btnShow(_ sender: UIButton) {
        if(showPass == true) {
            txtPassword.isSecureTextEntry = false
            sender.setTitle("Hide", for: .normal)
            showPass = false
        } else {
            txtPassword.isSecureTextEntry = true
            sender.setTitle("Show", for: .normal)
            showPass = true
        }
    }
    
    @IBAction func btnForgotPass(_ sender: UIButton) {
        let forgotVC = storyboard?.instantiateViewController(withIdentifier: "ForgotPassViewController")
        self.navigationController?.pushViewController(forgotVC!, animated: true)
    }
    
    @objc func pushVC() {
        let MapVC = storyboard?.instantiateViewController(withIdentifier: "MapViewController")
        self.navigationController?.pushViewController(MapVC!, animated: true)
    }
    
    func validateEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    
    func validatePassword(password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z0-9])[0-9A-Za-z\\d$@$#!%*?&]{4,}")
        return passwordTest.evaluate(with: password)
    }
    
    
    @IBAction func btnFbLogin(_ sender: UIButton) {
                let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
                    if (error == nil){
                        let fbloginresult : FBSDKLoginManagerLoginResult = result!
                        if fbloginresult.grantedPermissions != nil {
                            if(fbloginresult.grantedPermissions.contains("email"))
                            {
                                self.getFBUserData()
                                fbLoginManager.logOut()
                            }
                        }
                        fbLoginManager.logOut()
                    }
//                    }else{
//                        print(error?.localizedDescription ?? (Any).self)
//                        print(error)
//                    }
                }
        
//        let loginManager = FBSDKLoginManager()
//        loginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (loginResult, error) in
//            if error != nil {
//                print(error!)
//            }
//            else {
//                print(loginResult as Any)
//                if ((loginResult?.grantedPermissions) != nil){
//                    if (loginResult?.grantedPermissions.contains("email"))! {
//                        DispatchQueue.main.async
//                            {
//                                //self.indicator.isHidden = false
//                        }
//                        //self.getFBUserData()
//                    }
//                }
//            }
//        }
    }

    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                }
            })
        }
    }
    
    @IBAction func btnGLogin(_ sender: UIButton) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
//class ContainerViewController: SlideMenuController {
//    
//    override func awakeFromNib() {
//        if let controller = self.storyboard?.instantiateViewControllerWithIdentifier("Main") {
//            self.mainViewController = controller
//        }
//        if let controller = self.storyboard?.instantiateViewControllerWithIdentifier("Left") {
//            self.leftViewController = controller
//        }
//        super.awakeFromNib()
//    }
//    
//}



