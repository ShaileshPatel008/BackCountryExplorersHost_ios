//
//  LeftMenuViewController.swift
//  BackCountryExplorers
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 ASPL. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    let menuArr = ["Map","Adventures","Profile","Bank Information","Calendar","Refer a friend","Feedback", "Terms & Conditions", "Logout"]
    
    
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell")
        let label = cell?.viewWithTag(5) as! UILabel
        label.text = menuArr[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        switch indexPath.row
        {
        case 0:
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.slideMenuController()?.changeMainViewController(mapVC, close: true)
            break
        case 1:
            let advVC = self.storyboard?.instantiateViewController(withIdentifier: "AdventureListViewController") as! AdventureListViewController
            self.slideMenuController()?.changeMainViewController(advVC, close: true)
            break
        case 2:
            let proVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.slideMenuController()?.changeMainViewController(proVC, close: true)
            break
        case 3:
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.slideMenuController()?.changeMainViewController(mapVC, close: true)
            break
        case 4:
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.slideMenuController()?.changeMainViewController(mapVC, close: true)
            break
        case 5:
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.slideMenuController()?.changeMainViewController(mapVC, close: true)
            break
        case 6:
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.slideMenuController()?.changeMainViewController(mapVC, close: true)
            break
        case 7:
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.slideMenuController()?.changeMainViewController(mapVC, close: true)
            break
        case 8:
            let mapVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.slideMenuController()?.changeMainViewController(mapVC, close: true)
            break
            
            
        default:
            break
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
