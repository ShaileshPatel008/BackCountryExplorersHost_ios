//
//  TableViewCellClass.swift
//  BackCountryExplorers
//
//  Created by Apple on 09/11/17.
//  Copyright © 2017 ASPL. All rights reserved.
//

import UIKit

class TableViewCellClass: UITableViewCell {

    @IBOutlet weak var btnAdvImage: UIImageView!
    @IBOutlet weak var lblAdvName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
